﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

    //moves an object to a point without changing its z value. just a quick shortcut.
    public static void setVec2Pos(this Transform transform, Vector2 pos)
    {
        transform.position = new Vector3(pos.x, pos.y, transform.position.z);
    }

    public static Vector2 position2(this Transform transform)
    {
        return new Vector2(transform.position.x, transform.position.y);
    }

    public static Vector2 changeMagnitude(this Vector2 vec, float newMagnitude)
    {
        return vec.normalized * newMagnitude;

    }
}

