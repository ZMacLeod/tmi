﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AStar
{
    public static int top;
    public static int left;
    public static int width;
    public static int height;

    static AStarNode[,] grid;
    public static bool[,] wallGrid;
    public static List<AStarNode> nodeList = new List<AStarNode>();


    public static void initGrid(int _left, int _top, int _width, int _height)
    {
        top = _top;
        left = _left;
        width = _width;
        height = _height;

        grid = new AStarNode[width, height];
        wallGrid = new bool[width, height];

        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");

        foreach (GameObject wall in walls)
        {
            int wallX = Mathf.RoundToInt(wall.transform.position.x) - left;
            int wallY = top - Mathf.RoundToInt(wall.transform.position.y);
            if (wallX >= 0 && wallX < width && wallY >= 0 && wallY < height)
            {
                wallGrid[wallX, wallY] = true;
            }
        }

        for (int a = 0; a < width; a++)
        {
            for (int b = 0; b < height; b++)
            {
                if (!wallGrid[a, b]) { grid[a, b] = new AStarNode(a, b); }
            }
        }
        foreach (AStarNode node in nodeList)
        {
            node.Connect();
        }



    }

    /*
    public static void testWallGrid() {
    for(int a = 0; a < width; a++) { for(int b = 0; b < height; b++)
            {

                if (wallGrid[a, b]) { GameObject test = GameObject.CreatePrimitive(PrimitiveType.Sphere); test.transform.position = new Vector3(a, b, 1); }
            } 
        }
    }*/



    public static List<Vector2> Pathfind(float maxDist, int startWorldX, int startWorldY, int targetWorldX, int targetWorldY)
    {
        int startX = startWorldX - left;
        int targetX = targetWorldX - left;
        int startY = top - startWorldY;
        int targetY = top - targetWorldY;

        if (startX < 0 || startX >= width || startY < 0 || startY >= height || targetX < 0 || targetY >= height) //break if out of bounds
        {
            return null;
        }


        List<AStarNode> openNodes = new List<AStarNode>();
        List<AStarNode> closedNodes = new List<AStarNode>();

        AStarNode node = getNodeAt(startX, startY); //break if starting node is invalid
        if (node == null) { return null; } else { openNodes.Add(node); }
        AStarNode endNode = getNodeAt(targetX, targetY); //break if ending node is invalid
        if (endNode == null) { return null; }


        do
        {
            openNodes = openNodes.OrderBy(x => x.totalCost).ToList(); //Start by using nodes with lowest total cost
            foreach (AStarNode.Connection connection in openNodes[0].connections)
            {
                float nextCost = openNodes[0].totalCost + connection.cost;
                if (nextCost < maxDist)
                {
                    if (openNodes.Contains(connection.node) && connection.node.totalCost > nextCost) //overwrite open node if this one is better
                    {
                        connection.node.parent = openNodes[0];
                        connection.node.totalCost = nextCost;
                    }
                    else if (closedNodes.Contains(connection.node) && connection.node.totalCost > nextCost) // overwrite closed node if this one is better
                    {
                        closedNodes.Remove(connection.node);
                        openNodes.Add(connection.node);
                        connection.node.parent = openNodes[0];
                        connection.node.totalCost = nextCost;
                    }
                    else if (!openNodes.Contains(connection.node) & !closedNodes.Contains(connection.node))
                    {
                        openNodes.Add(connection.node);
                        connection.node.parent = openNodes[0];
                        connection.node.totalCost = nextCost;
                    }
                }
            }
            closedNodes.Add(openNodes[0]);
            openNodes.RemoveAt(0);
        } while (openNodes.Count > 0);

        if (closedNodes.Contains(endNode))
        {
            List<Vector2> list = endNode.backPropogate();
            NodeFlush();
            return list;
        }
        NodeFlush();
        return null;


    }

    public static void NodeFlush()
    {
        foreach (AStarNode node in nodeList) { node.parent = null; node.totalCost = 0; }
    }

    public static AStarNode getNodeAt(int x, int y)
    {
        if (x < 0 || x >= width || y < 0 || y >= height) { return null; }
        else { return grid[x, y]; }
    }

    public static bool diagCheck(int x, int y, int a, int b) {
        a = Mathf.Clamp(a, -1, 1);
        b = Mathf.Clamp(b, -1, 1);

        if (x < 0 || x >= width || y < 0 || y >= height) { return false; } //return false if location is out of bounds
        if (x+a < 0 || x+a >= width || y+b < 0 || y+b >= height) { return false; } //return false if target is out of bounds

        if(wallGrid[x,y] || wallGrid[x + a, y] || wallGrid[x, y + b] || wallGrid[x + a, y + b]) { return false; } //return false if there is a wall at the start, end, or in between.

        return true;
    }

}


public class AStarNode
{
    public int x, y; //these do NOT correlate with world space. Be careful!


    //These two values are only used during pathfinding. Whatever value they have afterwards is kept, though. 
    public AStarNode parent; //Used when tracing path from goal to origin.
    public float totalCost; //Used to get total cost to this point.

    public class Connection
    {
        public float cost; public AStarNode node;
        public Connection(float _cost, AStarNode _node)
        {
            cost = _cost;
            node = _node;
        }
    }
    public List<Connection> connections = new List<Connection>();

    public Vector2 getPos() // use this to get node position in world space!
    {
        return new Vector2(x + AStar.left, AStar.top - y);

    }

    public AStarNode(int _x, int _y)
    {
        x = _x;
        y = _y;
        AStar.nodeList.Add(this);
    }



    public List<Vector2> backPropogate()
    {
        return backPropogate(new List<Vector2>());

    }
    public List<Vector2> backPropogate(List<Vector2> nodeList)
    {
        nodeList.Insert(0, getPos());
        if (parent != null) { nodeList = parent.backPropogate(nodeList); }
        return nodeList;

    }


    public void Connect()
    {
        for (int a = -1; a <= 1; a++)
        {
            for (int b = -1; b <= 1; b++)
            {
                AStarNode node = AStar.getNodeAt(x + a, y + b);
                

                    if (node != null && node != this && AStar.diagCheck(x,y,a,b))
                    {
                        connections.Add(new Connection(new Vector2(a, b).magnitude, node));
                    }
            }
        }

    }

}