﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{

    float points { get { return getPoints(); } }

    [SerializeField] float width;
    [SerializeField] float height;

    Transform up;
    Transform left;
    Transform right;
    Transform down;

    int team;



    void Start()
    {
        left = transform.GetChild(0).GetChild(0);
        right = transform.GetChild(0).GetChild(1);
        up = transform.GetChild(0).GetChild(2);
        down = transform.GetChild(0).GetChild(3);

    }

    // Update is called once per frame
    float getPoints()
    {

        float _points = 0;
        Collider2D[] results;
        foreach (Collider2D col in Physics2D.OverlapBoxAll(transform.position, new Vector2(2f, 2f), 0f))
        {
            Item item = col.GetComponent<Item>();
            if (item != null && item.canBePickedUp || item.isHeld)
            {
                _points += item.pointValue;

            }
        }

        return _points;

    }


    private void OnValidate()
    {

        left = transform.GetChild(0).GetChild(0);
        right = transform.GetChild(0).GetChild(1);
        up = transform.GetChild(0).GetChild(2);
        down = transform.GetChild(0).GetChild(3);


        left.transform.localPosition = new Vector3(-width / 2f + 0.1f, 0f, 0f);
        right.transform.localPosition = new Vector3(width / 2f - 0.1f, 0f, 0f);
        up.transform.localPosition = new Vector3(0f, height/2f-0.1f, 0f);
        down.transform.localPosition = new Vector3(0f, -height / 2f + 0.1f, 0f);

        left.transform.localScale = new Vector3(0.2f,height, 1f);
        right.transform.localScale = new Vector3(0.2f, height, 1f);
        up.transform.localScale = new Vector3(width,0.2f, 1f);
        down.transform.localScale = new Vector3(width, 0.2f, 1f);

    }


    void Update() { Debug.Log(points); }
}
