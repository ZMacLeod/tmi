﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] public float pointValue;
    [SerializeField] public float speedMult; //dumb idea, multiplies player speed by a fraction if being held. (Or a bonus?)

    public bool canBePickedUp = true;

    public bool isHeld;

    protected float currentHeight;
    float verticalSpeed;

    float gravity = 10f;

    float rotSpeed;

    protected Transform sprite;

    public bool heldBehind = false;

    void Start() { sprite = transform.GetChild(0); thisStart(); }
    public virtual void thisStart()
    {

    }
    void Update()
    {
        thisUpdate();
        currentHeight = Mathf.Max(0f, currentHeight + verticalSpeed * Time.deltaTime);
        sprite.localPosition = new Vector3(0f, currentHeight, 0f);
        if (currentHeight > 0)
        {
            verticalSpeed -= gravity * Time.deltaTime;
            sprite.rotation = Quaternion.Euler(0f, 0f, sprite.rotation.eulerAngles.z + rotSpeed * Time.deltaTime);
            canBePickedUp = false;
        }
        else { canBePickedUp = true; }
    }


    public virtual void thisUpdate()
    {

    }

    public void Throw(float newVerticalSpeed)
    {
        verticalSpeed = newVerticalSpeed;
        rotSpeed = Random.Range(280, 450) * (Random.value >= 0.5f ? 1f : -1f);
        Debug.Log(rotSpeed);
    }

}
