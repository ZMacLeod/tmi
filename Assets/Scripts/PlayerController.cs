﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] float moveSpeed;

    Vector2 knockback;
    [SerializeField] float knockbackFriction;

    [SerializeField] float dashSpeed;
    [SerializeField] float maxDashDuration;
    [SerializeField] float maxDashCooldown;

    float dashCooldown;
    float dashDuration;

    Vector2 dashDir;

    float speedMod { get { if (isHoldingItem) { return heldItem.speedMult; } else { return 1f; } } } //gets modified movement speed

    float modMoveSpeed { get {  return moveSpeed * speedMod;  } } //gets modified movement speed

    float radius { get { return GetComponent<CircleCollider2D>().radius; } }
    bool isHoldingItem { get { return heldItem != null; } set { if (value == false) { heldItem = null; } } }
    Vector2 dir;



    Item heldItem;

    int team;




    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        handleMovement();
        handlePickup();
        handleHoldingItem();
        handleDash();

        if (Input.GetKeyDown(KeyCode.Space))
        {
           knockBack(Random.insideUnitCircle.normalized * 25f);
        }

    }

    void handleDash()
    {
        dashDuration = Mathf.Max(0f, dashDuration - Time.deltaTime);
        if (dashDuration <= 0)
        {
            dashCooldown = Mathf.Max(0f, dashCooldown - Time.deltaTime);
        }
    }


    void startDash()
    {
        if (Input.GetKeyDown(KeyCode.X) && dashCooldown<=0)
        {
            dashCooldown = maxDashCooldown;
            dashDuration = maxDashDuration;
            dashDir = dir;
        }
    }

    void knockBack(Vector2 knockbackVec)
    {
        if (isHoldingItem) { heldItem.Throw(7f); heldItem.isHeld = false; }
        isHoldingItem = false;
        dashDuration = 0f;
        knockback = knockbackVec;

    }


    void handleMovement()
    { //handles the movement
        Vector2 move = Vector2.zero;
        if (dashDuration <= 0f) { move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); }
        if (move != Vector2.zero)
        {
            move = move.normalized * modMoveSpeed * Time.deltaTime;
            dir = move.normalized;
            startDash();
        }
        move += knockback * Time.deltaTime;
        knockback = knockback.changeMagnitude(Mathf.Max(0, knockback.magnitude - knockbackFriction * Time.deltaTime));

        if (dashDuration > 0f)
        {
            move += dashDir * dashSpeed *speedMod * Time.deltaTime;
        }


        Vector2 horMove = new Vector2(move.x, 0);
        Vector2 verMove = new Vector2(0, move.y);

        move = new Vector2(snapMovement(horMove).x, snapMovement(verMove).y);

        transform.Translate(move);


    }

    Vector2 snapMovement(Vector2 vec) //checks if movement is about to hit wall, shortens if yes.
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radius + 0.01f /*buffer*/, vec, vec.magnitude, LayerMask.GetMask("Walls"));
        if (hits.Length > 0)
        {
            float furthestDist = vec.magnitude;
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.distance < furthestDist && Vector2.Dot(hit.normal, vec) < 0) { furthestDist = hit.distance; }
            }
            return vec.normalized * furthestDist;
        }
        return vec;
    }


    void handlePickup()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!isHoldingItem)
            {
                Collider2D[] results;
                foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, radius))
                {
                    Item item = col.GetComponent<Item>();
                    if (item != null && item.canBePickedUp)
                    {

                        heldItem = item;
                        heldItem.isHeld = true;
                        return;
                    }
                }
            }
            else
            {
                heldItem.isHeld = false;
                isHoldingItem = false;
            }
        }


    }

    void handleHoldingItem()
    {
        if (isHoldingItem)
        {
            heldItem.transform.setVec2Pos(transform.position2() + dir * radius);
        }
    }
}
