﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingItem : Item
{

    [SerializeField] float minShutdownTime; //how long it shuts down after being dropped
    [SerializeField] float maxShutdownTime;
    [SerializeField] float movementSpeed;
    [SerializeField] float minChangeDir; //how long until it changes direction while wandering
    [SerializeField] float maxChangeDir;

    float shutdown;
    float changeDirTimer;
    Vector2 dir;

    [SerializeField] Color offColor;
    [SerializeField] Color onColor;
    SpriteRenderer sr;

    float radius { get { return GetComponent<CircleCollider2D>().radius; } }

    public override void thisStart()
    {
        sr = GetComponentInChildren<SpriteRenderer>();
    }

    public override void thisUpdate()
    {

        if (shutdown <= 0)
        {
            sr.color = onColor;
            Vector2 move = dir * movementSpeed * Time.deltaTime;
            float horMove = snapMovement(new Vector2(move.x, 0)).x;
            float verMove = snapMovement(new Vector2(0, move.y)).y;
            transform.Translate(new Vector2(horMove, verMove));
            sprite.rotation = Quaternion.LookRotation(Vector3.forward,dir);
            changeDirTimer -= Time.deltaTime;
            if (changeDirTimer <= 0f)
            {
                changeDirTimer = Random.Range(minChangeDir, maxChangeDir);
                dir = Random.insideUnitCircle.normalized;
            }

        }
        else
        {
            if (shutdown < 1f && (Mathf.RoundToInt(shutdown * 12) % 2 == 0))
            {
                sr.color = onColor;
            }
            else
            {
                sr.color = offColor;
            }
        }
        if (!isHeld && currentHeight==0) { shutdown -= Time.deltaTime; }
        else
        {
            shutdown = Random.Range(minShutdownTime, maxShutdownTime);
        }


    }

    Vector2 snapMovement(Vector2 vec) //checks if movement is about to hit wall, shortens if yes.
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radius + 0.01f /*buffer*/, vec, vec.magnitude, LayerMask.GetMask("Walls"));
        if (hits.Length > 0)
        {
            float furthestDist = vec.magnitude;
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.distance < furthestDist && Vector2.Dot(hit.normal, vec) < 0) { furthestDist = hit.distance; }
            }
            return vec.normalized * furthestDist;
        }
        return vec;
    }


}
