﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonContainer : MonoBehaviour
{
    Vector2 oldPos;

    [SerializeField] float dist;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = oldPos;
        transform.localPosition = transform.localPosition.normalized * dist;
        transform.rotation = Quaternion.LookRotation(-transform.localPosition, Vector3.forward);

        oldPos = transform.position;
    }
}
