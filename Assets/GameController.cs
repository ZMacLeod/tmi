﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [SerializeField] float gameStartTime;

    public static List<Team> teams;
    


    // Start is called before the first frame update
    void Start()
    {
    

        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public  class Team {

    public List<Goal> goals;
    public List<PlayerController> players;
    public Color color;

    public int playerCount { get { return players.Count; } }

}